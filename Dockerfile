ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_python:3.8.2 AS opt_python

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_tools:19.04 AS build

COPY --from=opt_python /opt /opt

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

RUN apt install -y libfreetype6-dev && \
    apt autoremove -y && \
    apt clean

RUN python -m pip install numpy
RUN python -m pip install multiqc

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_base:19.04

COPY --from=build /opt /opt
COPY --from=build /usr/lib/x86_64-linux-gnu/libfreetype.so.6 /opt/lib/
COPY --from=build /usr/lib/x86_64-linux-gnu/libpng16.so.16 /opt/lib/

ENV PATH /opt/bin/:/usr/bin/:/bin/:/usr/sbin/:/sbin/
ENV LD_LIBRARY_PATH /opt/lib/

RUN mkdir -p /usr/bin/ && echo "#!/bin/bash" > /usr/bin/module && chmod a+x /usr/bin/module
